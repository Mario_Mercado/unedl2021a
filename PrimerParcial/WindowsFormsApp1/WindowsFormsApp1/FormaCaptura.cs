﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class FormaCaptura : Form
    {
        string[,] personas = new string[,]
        { { "Angel","Duran" },
          { "Christopher","Villalobos" },
          { "Daniel","Lopez" },
          { "Daniel","Vazquez" },
          { "Edgar","Banuelos" },
          { "Fernando","Hernandez" },
          { "Francisco","Garcia" },
          { "Ivan","Narvaez" },
          { "Joel","Juarez" },
          { "Juan","Romero" },
          { "Kevin","Gonzalez" },
          { "Luis","Gomez" },
          { "Manuel","Mariscal" },
          { "Mario","Mercado" },
          { "Marisol","Benitez" },
          { "Mauricio","Castaneda" },
          { "Oscar","Ochoa" },
          { "Yahayra","Rodriguez" },
          { "Cesar", "De la Cruz" }
        };

        public FormaCaptura()
        {
            InitializeComponent();
        }

        private void btnBuscar_Click(object sender, EventArgs e)
        {
            string nombre = txtBuscaNombre.Text;
            string apellido = txtBuscaApellido.Text;
            int C = 0;
            for (int A=0;A<18;A++)
            {

                if (String.Compare(personas[A, 0], nombre) == 0 || String.Compare(personas[A, 1], apellido) == 0)
                {

                    MessageBox.Show( personas[A,0]  +  personas [A,1] , "Persona encontrada");
                        C = 1;                    
                    }
            }
            if (C==0)
            {
                MessageBox.Show("Persona NO encontrada");
            } 
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            txtBuscaApellido.Text = "";
            txtBuscaNombre.Text = "";
                    }
    }
}
