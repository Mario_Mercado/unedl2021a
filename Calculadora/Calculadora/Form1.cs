﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculadora
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private object Form1_Load(object sender, EventArgs e)
        {
            Resultado.Text = "0";
            txtnum1.Text = "0";
            txtnum2.Text = "0";
           
        }

        private void Botres_Click(object sender, EventArgs e)
        {

            double A = Convert.ToDouble(txtnum1.Text);
            double B = Convert.ToDouble(txtnum2.Text);
            double C = A - B;
            Resultado.Text = C.ToString();
        }

        private void BotSuma_Click(object sender, EventArgs e)
        {
            double A = Convert.ToDouble(txtnum1.Text);
            double B = Convert.ToDouble(txtnum2.Text);
            double C = A + B;
            Resultado.Text = C.ToString();


        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Resultado_Click(object sender, EventArgs e)
        {

        }

        private void Botclear_Click(object sender, EventArgs e)
        {
            Resultado.Text = "0";
            txtnum1.Text = "0";
            txtnum2.Text = "0";
        }

        private void Botmult_Click(object sender, EventArgs e)
        {

            double A = Convert.ToDouble(txtnum1.Text);
            double B = Convert.ToDouble(txtnum2.Text);
            double C = A * B;
            Resultado.Text = C.ToString();
        }

        private void Botdiv_Click(object sender, EventArgs e)
        {

            double A = Convert.ToDouble(txtnum1.Text);
            double B = Convert.ToDouble(txtnum2.Text);
            double C = A / B;
            if (C==0)
            {
                
            }
            Resultado.Text = C.ToString();
        }
    }
}
