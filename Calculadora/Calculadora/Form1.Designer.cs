﻿
namespace Calculadora
{
    partial class Form1
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BotSuma = new System.Windows.Forms.Button();
            this.Botres = new System.Windows.Forms.Button();
            this.Valor2 = new System.Windows.Forms.Label();
            this.Valor1 = new System.Windows.Forms.Label();
            this.Resultadomuestra = new System.Windows.Forms.Label();
            this.Resultado = new System.Windows.Forms.Label();
            this.Botdiv = new System.Windows.Forms.Button();
            this.Botmult = new System.Windows.Forms.Button();
            this.txtnum1 = new System.Windows.Forms.TextBox();
            this.txtnum2 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.Botclear = new System.Windows.Forms.Button();
            this.div0 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // BotSuma
            // 
            this.BotSuma.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.BotSuma.Location = new System.Drawing.Point(479, 36);
            this.BotSuma.Name = "BotSuma";
            this.BotSuma.Size = new System.Drawing.Size(120, 56);
            this.BotSuma.TabIndex = 0;
            this.BotSuma.Text = "+";
            this.BotSuma.UseVisualStyleBackColor = true;
            this.BotSuma.Click += new System.EventHandler(this.BotSuma_Click);
            // 
            // Botres
            // 
            this.Botres.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Botres.Location = new System.Drawing.Point(479, 108);
            this.Botres.Name = "Botres";
            this.Botres.Size = new System.Drawing.Size(120, 56);
            this.Botres.TabIndex = 1;
            this.Botres.Text = "-";
            this.Botres.UseVisualStyleBackColor = true;
            this.Botres.Click += new System.EventHandler(this.Botres_Click);
            // 
            // Valor2
            // 
            this.Valor2.AutoSize = true;
            this.Valor2.Font = new System.Drawing.Font("Segoe UI Symbol", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Valor2.Location = new System.Drawing.Point(12, 119);
            this.Valor2.Name = "Valor2";
            this.Valor2.Size = new System.Drawing.Size(134, 50);
            this.Valor2.TabIndex = 2;
            this.Valor2.Text = "Valor2";
            // 
            // Valor1
            // 
            this.Valor1.AutoSize = true;
            this.Valor1.Font = new System.Drawing.Font("Segoe UI Symbol", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Valor1.Location = new System.Drawing.Point(12, 69);
            this.Valor1.Name = "Valor1";
            this.Valor1.Size = new System.Drawing.Size(145, 50);
            this.Valor1.TabIndex = 3;
            this.Valor1.Text = "Valor 1";
            this.Valor1.Click += new System.EventHandler(this.label2_Click);
            // 
            // Resultadomuestra
            // 
            this.Resultadomuestra.AutoSize = true;
            this.Resultadomuestra.Font = new System.Drawing.Font("Segoe UI Symbol", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Resultadomuestra.Location = new System.Drawing.Point(12, 174);
            this.Resultadomuestra.Name = "Resultadomuestra";
            this.Resultadomuestra.Size = new System.Drawing.Size(194, 50);
            this.Resultadomuestra.TabIndex = 4;
            this.Resultadomuestra.Text = "Resultado";
            // 
            // Resultado
            // 
            this.Resultado.AutoSize = true;
            this.Resultado.Font = new System.Drawing.Font("Segoe UI Symbol", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Resultado.Location = new System.Drawing.Point(243, 174);
            this.Resultado.Name = "Resultado";
            this.Resultado.Size = new System.Drawing.Size(126, 50);
            this.Resultado.TabIndex = 5;
            this.Resultado.Text = "label4";
            this.Resultado.Click += new System.EventHandler(this.Resultado_Click);
            // 
            // Botdiv
            // 
            this.Botdiv.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Botdiv.Location = new System.Drawing.Point(479, 235);
            this.Botdiv.Name = "Botdiv";
            this.Botdiv.Size = new System.Drawing.Size(120, 56);
            this.Botdiv.TabIndex = 7;
            this.Botdiv.Text = "/";
            this.Botdiv.UseVisualStyleBackColor = true;
            this.Botdiv.Click += new System.EventHandler(this.Botdiv_Click);
            // 
            // Botmult
            // 
            this.Botmult.Font = new System.Drawing.Font("Segoe UI", 27.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.Botmult.Location = new System.Drawing.Point(479, 174);
            this.Botmult.Name = "Botmult";
            this.Botmult.Size = new System.Drawing.Size(120, 56);
            this.Botmult.TabIndex = 6;
            this.Botmult.Text = "*";
            this.Botmult.UseVisualStyleBackColor = true;
            this.Botmult.Click += new System.EventHandler(this.Botmult_Click);
            // 
            // txtnum1
            // 
            this.txtnum1.Location = new System.Drawing.Point(178, 82);
            this.txtnum1.Name = "txtnum1";
            this.txtnum1.Size = new System.Drawing.Size(86, 23);
            this.txtnum1.TabIndex = 8;
            this.txtnum1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // txtnum2
            // 
            this.txtnum2.Location = new System.Drawing.Point(178, 141);
            this.txtnum2.Name = "txtnum2";
            this.txtnum2.Size = new System.Drawing.Size(86, 23);
            this.txtnum2.TabIndex = 9;
            // 
            // button1
            // 
            this.button1.Image = global::Calculadora.Properties.Resources._167049336_3476927735746753_3017441021481174271_n;
            this.button1.Location = new System.Drawing.Point(12, 296);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(160, 84);
            this.button1.TabIndex = 10;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // Botclear
            // 
            this.Botclear.Font = new System.Drawing.Font("Segoe UI", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point);
            this.Botclear.Location = new System.Drawing.Point(479, 300);
            this.Botclear.Name = "Botclear";
            this.Botclear.Size = new System.Drawing.Size(120, 56);
            this.Botclear.TabIndex = 11;
            this.Botclear.Text = "C";
            this.Botclear.UseVisualStyleBackColor = true;
            this.Botclear.Click += new System.EventHandler(this.Botclear_Click);
            // 
            // div0
            // 
            this.div0.Font = new System.Drawing.Font("Segoe UI", 48F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point);
            this.div0.Location = new System.Drawing.Point(110, 12);
            this.div0.Name = "div0";
            this.div0.Size = new System.Drawing.Size(602, 391);
            this.div0.TabIndex = 12;
            this.div0.Text = "Un numero no se puede dividir entre 0";
            this.div0.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.div0);
            this.Controls.Add(this.Botclear);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.txtnum2);
            this.Controls.Add(this.txtnum1);
            this.Controls.Add(this.Botdiv);
            this.Controls.Add(this.Botmult);
            this.Controls.Add(this.Resultado);
            this.Controls.Add(this.Resultadomuestra);
            this.Controls.Add(this.Valor1);
            this.Controls.Add(this.Valor2);
            this.Controls.Add(this.Botres);
            this.Controls.Add(this.BotSuma);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BotSuma;
        private System.Windows.Forms.Button Botres;
        private System.Windows.Forms.Label Valor2;
        private System.Windows.Forms.Label Valor1;
        private System.Windows.Forms.Label Resultadomuestra;
        private System.Windows.Forms.Label Resultado;
        private System.Windows.Forms.Button Botdiv;
        private System.Windows.Forms.Button Botmult;
        private System.Windows.Forms.TextBox txtnum1;
        private System.Windows.Forms.TextBox txtnum2;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button Botclear;
        private System.Windows.Forms.Button div0;
    }
}

